var express = require("express");
var app = express();


const logger = require('winston');
require('winston-logstash');

logger.add(logger.transports.Logstash,
    {
        port: 10329,
        host: 'dc90437c-6845-4668-80da-5e5611b8bee0-ls.logit.io',
        ssl_enable: true,
        max_connect_retries: -1,
    });

app.post("/echo", (req, res) => {
    const { param } = req.query;
    logger.info(param);

    return res.json({ echo: param });
});

app.listen(3000, () => {
    console.log("Server running on port 3000");
});